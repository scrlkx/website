/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    './src/**/*.{html,js,ts,jsx,tsx}',
  ],
  theme: {
    colors: {
      current: 'currentColor',
      'dark': '#282828',
      'light': '#FBF1C7',
      'fg-dark': '#EBDBB2',
      'fg-light': '#3C3836',
      'fg0-light': '#282828',
      'fg0-dark': '#FBF1C7',
      'fg2-dark': '#D5C4A1',
      'fg2-light': '#504945',
      'fg3-dark': '#BDAE93',
      'fg3-light': '#665C54',
      'fg4-dark': '#A89984',
      'fg4-light': '#7C6F664',
      'green-dark': '#98971A',
      'green-light': '#98971A',
      'yellow-dark': '#D79921',
      'yellow-light': '#D79921',
    },
    fontFamily: {
      sans: ['Inter', 'ui-sans-serif', 'system-ui'],
    },
  },
  plugins: [],
}
